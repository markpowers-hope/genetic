﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GeneticSharp.Domain.Chromosomes;

namespace GeneticSharp.Runner.ConsoleApp
{
    class MyChromosome : ChromosomeBase
    {
        public MyChromosome() : base(8)
        {
            CreateGenes();
        }

        public override Gene GenerateGene(int geneIndex)
        {
            Random r = new Random();
            return new Gene(r.Next(2));
        }

        public override IChromosome CreateNew()
        {
           return new MyChromosome();
        }
    }
}
