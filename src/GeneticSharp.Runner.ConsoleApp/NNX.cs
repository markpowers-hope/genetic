﻿using System;
using GeneticSharp.Domain.Crossovers;
using System.Collections.Generic;
using System.Collections;
using GeneticSharp.Domain.Chromosomes;
using System.Linq;

namespace GeneticSharp.Runner.ConsoleApp
{
	public class NNX : CrossoverBase
	{
		public static int[][] GRAPH; 

		public NNX () : base(2, 1)
		{
			
		}

		protected override IList<IChromosome> PerformCross (IList<IChromosome> parents)
		{
			List<IChromosome> children = new List<IChromosome> ();

			Random r = new Random ();

			IChromosome parent1 = parents [0];
			IChromosome parent2 = parents [1];

			List<Gene> childPath = new List<Gene> ();

			int currentVertex = r.Next (parent1.Length);
			List<Gene> p1Path = parent1.GetGenes().OfType<Gene>().ToList();
			List<Gene> p2Path = parent2.GetGenes().OfType<Gene>().ToList();

			childPath.Add (new Gene (currentVertex));

			// Do the nearest neighbor algorithm
			while (childPath.Count < p1Path.Count) {
				int index1 = p1Path.IndexOf (new Gene (currentVertex));
				Gene option1 = new Gene (p1Path [(index1 + 1) % GRAPH.Length].Value);

				int index2 = p2Path.IndexOf (new Gene (currentVertex));
				Gene option2 = new Gene (p2Path [(index2 + 1) % GRAPH.Length].Value);


				if (index1 < 0 || index2 < 0) {
					Console.WriteLine ("Current vertex ({0}) not in parents path", currentVertex);
				}

				if (childPath.Contains (option1) && childPath.Contains (option2)) {

					int[] normalP = new int[GRAPH.Length];
					for (int i = 0; i < normalP.Length; i++)
					{
						normalP[i] = i;
					}

					int[] randomP = normalP.OrderBy(x => r.Next()).ToArray();

					for (int i = 0; i < randomP.Length; i++) {
						Gene temp = new Gene (randomP[i]);
						if (!childPath.Contains (temp)) {
							childPath.Add (temp);
							break;
						}
					}
				} else if (childPath.Contains (option1)) {
					currentVertex = (int)option2.Value;
					childPath.Add (option2);
				} else if (childPath.Contains (option2)) {
					currentVertex = (int)option1.Value;
					childPath.Add (option1);
				} else {
					if (index1 < 0) {
						currentVertex = (int)option2.Value;
						childPath.Add (option2);
					} else if (index2 < 0) {
						currentVertex = (int)option1.Value;
						childPath.Add (option1);
					} else if (edgeWeight (p1Path, index1) < edgeWeight (p2Path, index2)) {
						currentVertex = (int)option1.Value;
						childPath.Add (option1);
					} else {
						currentVertex = (int)option2.Value;
						childPath.Add (option2);
					}
				}

				var childSet = new HashSet<Gene> (childPath);

				if (childSet.Count != childPath.Count) {
					Console.WriteLine ("uh oh");
				}

			}



			MyTSPChromosome child = new MyTSPChromosome ();
			for (int i = 0; i < childPath.Count; i++)
			{
				child.ReplaceGene(i, childPath[i]);
			}

			children.Add(child);


			return children;
		}

		private int edgeWeight(List<Gene> path, int index){
			return GRAPH[(int)path[index].Value][(int)path[(index+1)%GRAPH.Length].Value];
		}
	}
}

