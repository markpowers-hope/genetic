﻿using System;
using GeneticSharp.Domain.Mutations;
using GeneticSharp.Domain.Chromosomes;

namespace GeneticSharp.Runner.ConsoleApp
{
	public class TSPMutation : MutationBase
	{
		public TSPMutation ()
		{
			IsOrdered = true;
		}

		protected override void PerformMutate(IChromosome chromosome, float probability)
		{
			Random r = new Random ();
			if (r.NextDouble() < probability) {
				int index1 = r.Next (chromosome.Length);
				int index2 = r.Next (chromosome.Length - index1) + index1;

				while(index1 < index2){
					Gene temp = chromosome.GetGene (index1);
					chromosome.ReplaceGene (index1, chromosome.GetGene (index2));
					chromosome.ReplaceGene (index2, temp);
					index1++;
					index2--;
				}
			}
		}
	}
}

