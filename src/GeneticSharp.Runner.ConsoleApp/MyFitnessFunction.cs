﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GeneticSharp.Domain.Chromosomes;
using GeneticSharp.Domain.Fitnesses;

namespace GeneticSharp.Runner.ConsoleApp
{
    class MyFitnessFunction : IFitness
    {
        public double Evaluate(IChromosome chromosome)
        {
            double value = 0;
            for (int i = 0; i < chromosome.Length; i++)
            {
                value *= 2;
                value += (int)chromosome.GetGene(i).Value;
            }
            return Math.Sin(value * Math.PI/256.0);
        }
    }
}
