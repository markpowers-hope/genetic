﻿using System;
using System.IO;
using GeneticSharp.Domain;
using GeneticSharp.Domain.Crossovers;
using GeneticSharp.Domain.Mutations;
using GeneticSharp.Domain.Populations;
using GeneticSharp.Domain.Selections;
using GeneticSharp.Domain.Terminations;
using GeneticSharp.Extensions.Tsp;
using GeneticSharp.Infrastructure.Framework.Reflection;
using GeneticSharp.Infrastructure.Threading;
using GeneticSharp.Runner.ConsoleApp.Samples;

namespace GeneticSharp.Runner.ConsoleApp
{
    public class Program
    {
        public static void Main(string[] args)
		{
			string[] files = new string[12];
			string[] outputFiles = new string[12];

			for (int i = 1; i <= 10; i++) {
				files [i - 1] = "../../TSPGraphs/K" + (i * 5).ToString ().PadLeft (2, '0') + ".txt";
				outputFiles [i - 1] = "../../TSPGraphs/OutputK" + (i * 5).ToString ().PadLeft (2, '0') + ".txt";
			}

			files [10] = "../../TSPGraphs/K09.txt";
			files [11] = "../../TSPGraphs/K12.txt";
			outputFiles [10] = "../../TSPGraphs/outputK09.txt";
			outputFiles [11] = "../../TSPGraphs/outputK12.txt";

			while (true) {
				for (int index = 2; index < files.Length; index++) {
					if (index == 10) {
						continue;
					}

					string fileName = files [index];
					string outputFileName = outputFiles [index];

					// Create adj matrix from file
					string[] lines = File.ReadAllLines (fileName);
					int VertexCount = lines.Length;
					int[][] adjMatrix = new int[VertexCount][];
					for (int i = 0; i < VertexCount; i++) {
						string[] stringStuff = lines [i].Split (' ');
						int[] intStuff = new int[stringStuff.Length];
						for (int u = 0; u < stringStuff.Length; u++) {
							intStuff [u] = int.Parse (stringStuff [u]);
						}
						adjMatrix [i] = intStuff;
					}

					// Print size of matrix
					Console.WriteLine ("Graph size {0}", adjMatrix.Length);

					// Set GRAPH field in required classes
					MyTSPChromosome.GRAPH = adjMatrix;
					MyTSPFitness.GRAPH = adjMatrix;
					NNX.GRAPH = adjMatrix;
		            
					// Initialize Genetic Algorithm
					var selection = new TopHalfSelection ();
					var crossover = new NNX (); 
					var mutation = new TSPMutation (); 
					var fitness = new MyTSPFitness (); 
					var chromosome = new MyTSPChromosome ();
					var population = new Population (200, 300, chromosome, true);
					var ga = new GeneticAlgorithm (population, fitness, selection, crossover, mutation);
					ga.Termination = new GenerationNumberTermination (1500);            
					ga.Start ();
		            
					// Print out answer for genetic algorithm
					string toWrite = "powers:" + adjMatrix.Length.ToString ().PadLeft (2, '0') + ":" + ga.BestOverallChromosome.Fitness + ":";

					Console.WriteLine ("Length " + ga.BestOverallChromosome.Fitness);
					for (int i = 0; i < ga.BestOverallChromosome.Length; i++) {
						string stuff = ga.BestOverallChromosome.GetGene (i).Value + " ";
						Console.Write (stuff);
						toWrite += stuff;
					}
					Console.WriteLine ("\n");
					File.AppendAllText (outputFileName, toWrite + "\n");

				}
			}
		}
    }
}
