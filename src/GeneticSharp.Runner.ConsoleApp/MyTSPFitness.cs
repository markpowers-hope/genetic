﻿using GeneticSharp.Domain.Chromosomes;
using GeneticSharp.Domain.Fitnesses;
using System;

namespace GeneticSharp.Runner.ConsoleApp
{
    internal class MyTSPFitness : IFitness
    {
		public static int[][] GRAPH;
        public MyTSPFitness()
        {
        }

        public double Evaluate(IChromosome chromosome)
        {
            double length = 0;
            for (int i = 0; i < chromosome.Length; i++)
            {
                int v1 = (int)chromosome.GetGene(i).Value;
                int v2 = (int)chromosome.GetGene((i + 1)%chromosome.Length).Value;
                length += GRAPH[v1][v2];
            }


            return length;
        }
    }
}