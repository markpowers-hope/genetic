﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using GeneticSharp.Domain.Chromosomes;

namespace GeneticSharp.Runner.ConsoleApp
{
    internal class MyTSPChromosome : ChromosomeBase
    {
        public static int[][] GRAPH;

        public MyTSPChromosome() : base(GRAPH.Length)
        {
			int[] path;
            path = new int[GRAPH.Length];
            for (int i = 0; i < path.Length; i++)
            {
                path[i] = i;
            }

            Random r = new Random();
            int[] randomP = path.OrderBy(x => r.Next()).ToArray();
            for (int i = 0; i < path.Length; i++)
            {
				ReplaceGene(i, new Gene(randomP[i]));
            }
        }

        public override IChromosome CreateNew()
        {
            return new MyTSPChromosome();
        }

        public override Gene GenerateGene(int geneIndex)
        {
            throw new NotImplementedException();
        }
    }
}