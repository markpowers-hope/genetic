using System;
using System.Collections.Generic;
using System.Linq;
using GeneticSharp.Domain.Chromosomes;
using HelperSharp;

namespace GeneticSharp.Domain.Populations
{
	/// <summary>
	/// Represents a generation of a population.
	/// </summary>
	public sealed class Generation
	{
		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="GeneticSharp.Domain.Populations.Generation"/> class.
		/// </summary>
		/// <param name="number">The generation number.</param>
		/// <param name="chromosomes">The chromosomes of the generation..</param>
		/// <param name="minimizeFitness">Set whether the fitness is sorted in ascending or descending order</param>
		public Generation(int number, IList<IChromosome> chromosomes, bool minimizeFitness = false)
		{
			if (number < 1)
			{
				throw new ArgumentOutOfRangeException(
					"number",
					"Generation number {0} is invalid. Generation number should be positive and start in 1.".With(number));
			}

			if (chromosomes == null || chromosomes.Count < 2)
			{
				throw new ArgumentOutOfRangeException("chromosomes", "A generation should have at least 2 chromosomes.");
			}

			Number = number;
			CreationDate = DateTime.Now;
			Chromosomes = chromosomes;
			MinimizeFitness = minimizeFitness;
		}
		#endregion

		#region Properties
		/// <summary>
		/// Control whether the best chromosome has high or low fitness
		/// </summary>
		/// <value>True if fitness is being minimized.</value>
		public bool MinimizeFitness { get; private set; }

		/// <summary>
		/// Gets the number.
		/// </summary>
		/// <value>The number.</value>
		public int Number { get; private set; }

		/// <summary>
		/// Gets the creation date.
		/// </summary>
		public DateTime CreationDate { get; private set; }

		/// <summary>
		/// Gets the chromosomes.
		/// </summary>
		/// <value>The chromosomes.</value>
		public IList<IChromosome> Chromosomes { get; internal set; }

		/// <summary>
		/// Gets the best chromosome.
		/// </summary>
		/// <value>The best chromosome.</value>
		public IChromosome BestChromosome { get; internal set; }

		#endregion

		#region Methods
		/// <summary>
		/// Ends the generation.
		/// </summary>
		/// <param name="chromosomesNumber">Chromosomes number to keep on generation.</param>
		public void End(int chromosomesNumber)
		{
			Chromosomes = Chromosomes
				.Where(c => ValidateChromosome(c))
				.OrderByDescending(c => c.Fitness.Value)
				.ToList();

			if (Chromosomes.Count > chromosomesNumber)
			{
				Chromosomes = Chromosomes.Take(chromosomesNumber).ToList();
			}
			if (MinimizeFitness)
			{
				BestChromosome = Chromosomes.Last();
			}else
			{
				BestChromosome = Chromosomes.First();
			}
		}

		/// <summary>
		/// Validates the chromosome.
		/// </summary>
		/// <param name="chromosome">The chromosome to validate.</param>
		/// <returns>True if a chromosome is valid.</returns>
		private static bool ValidateChromosome(IChromosome chromosome)
		{
			if (!chromosome.Fitness.HasValue)
			{
				throw new InvalidOperationException("There is unknown problem in current generation, because a chromosome has no fitness value.");
			}

			return true;
		}
		#endregion
	}
}